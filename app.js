/*--------------LOCAL STORAGE DANS NAVIGATEUR -------------------*/ 
$("button").on('click',function(){

    var fiche = {                                // mettre toute les valeur entrer dans l'input ...
        nom : $("#name").val(),                 // .. dans un tableau  format objet sous JSON
        prenom : $("#prenom").val(),
        login : $("#login").val(),
        sexeF:$('input[name=sexeF]:checked').val(),
        sexeH:$('input[name=sexeH]:checked').val(),
        mdp : $("#mdp").val(),
        DateNaissance : $("#DateNaissance").val(),
        ville : $("#ville").val(),
        email : $("#email").val(),
        url : $("#url").val(),
        hobbies : $("#hobbies").val(),
        telephone : $("#telephone").val(),
        color : $("#color").val(),
      }

    var annuaire = localStorage.getItem('annuaire',fiche);  
    if (annuaire == null){
        var annuaire = []
    }
    annuaire = JSON.parse('annuaire')
    annuaire.push(fiche);
    localStorage.setItem("annuaire", JSON.stringify(fiche));  // transformer les element objet en string
 
})

/*--------------CONDITION MOT DE PASSE-------------------*/ 

function CheckForm(){               
var mdp = document.getElementById("mdp").value;
var listRe = [{                         // mettre condition dans tableau
                re: /[0-9]{1}/,
                msg: "Votre mot de passe doit avoir au moins 1 chiffres"
                },
                {
                re: /[^A-Za-z0-9]/,
                msg: "Votre mot de passe doit posséder au moins 1 caractère spéciale"
                }
                ];
    for (var i = 0; i < listRe.length; i++) { // parcourir le tableau 
        var item = listRe[i];    
        if (!item.re.test(mdp)) {  // condition non rempli
            alert(item.msg);       // envoye message d'alerte 
            return false;
        }
    }
}
